//Include gulp
var gulp = require('gulp');

//Include plugins
var sass = require('gulp-sass'),
	minifyCSS = require('gulp-minify-css'),
	minifyHTML = require('gulp-minify-html'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	jshint = require('gulp-jshint'),
	addsrc = require('gulp-add-src'),
	livereload = require('gulp-livereload');


//Task input
var style = {
		watch: [
			'src/**/*.scss'
		],
		src: [
			'src/style/style.scss'
		]
	},
	script = {
		watch: [
			'src/**/*.js'
		],
		src: [
			'src/script/**/*.js'
		],
		src_ext: [
			'src/ext/jquery-2.1.1.min.js'
		]
	},
	html = {
		watch: [
			'src/html/**/*.html'
		],
		src: [
			'src/html/**/*.html'
		]
	};


//Pipelines
gulp.task('style', function () {
	gulp.src(style.src)
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(gulp.dest('dist'))
		.pipe(livereload({auto: false}));
});

gulp.task('script', function () {
	gulp.src(script.src)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(uglify({mangle: false}))
		.pipe(addsrc.prepend(script.src_ext))
		.pipe(concat('script.js'))
		.pipe(gulp.dest('dist'))
		.pipe(livereload({auto: false}));
});

gulp.task('html', function () {
	gulp.src(html.src)
		.pipe(minifyHTML())
		.pipe(gulp.dest('dist'))
		.pipe(livereload({auto: false}));
});

//Tasks
gulp.task('build', [
	'style',
	'script',
	'html'
]);

gulp.task('watch', function () {
	livereload.listen();

	gulp.watch(style.watch, ['style']);
	gulp.watch(script.watch, ['script']);
	gulp.watch(html.watch, ['html']);
});

//Default
gulp.task('default', ['watch']);